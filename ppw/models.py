from django.db import models

class Restoran(models.Model):
    nama_restoran = models.CharField(max_length=100, default='Unknown Restaurant')
    lokasi = models.CharField(max_length=100, default='Jakarta')
    kategori = models.CharField(max_length=100, default='Fast Food')
    foto = models.CharField(max_length=255, default='none')
    rating = models.DecimalField(max_digits=2, decimal_places=1, default='0.0')
    def __str__(self):
        return self.nama_restoran

class Makanan(models.Model):
    nama_makanan = models.CharField(max_length=100, default='None')
    restoran = models.ForeignKey(Restoran, on_delete=models.CASCADE)
    harga = models.IntegerField(default=0)
    foto = models.CharField(max_length=255, default='none')
    def __str__(self):
        return self.nama_makanan