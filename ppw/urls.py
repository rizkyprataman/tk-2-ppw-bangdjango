from django.urls import path
from . import views

app_name = 'ppw'

urlpatterns = [
    path('', views.index, name='index'),
]
