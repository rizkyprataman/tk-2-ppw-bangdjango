from django.shortcuts import render
from .models import Restoran, Makanan
from kybe.forms import Pencarian
from kybe.views import restoran, cariResto
from kybe.models import Review

def tambahRating():
    show = Restoran.objects.all()
    for resto in show:
        pk = resto
        review = Review.objects.filter(resto=pk)
        nilai = 0
        jumlah = 0
        for rate in review:
            nilai += rate.rating
            jumlah += 1
        nilai /= jumlah
        nilai = round(nilai, 1)
        resto.rating = nilai
        resto.save()

def index(request):
    if request.method ==  "GET":
        tambahRating()
        show = Restoran.objects.all()
        form = Pencarian
        args = {'show':show, 'form':form}
        return render(request, 'index.html', args)
    else:
        return(restoran(request))