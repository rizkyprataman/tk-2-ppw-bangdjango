from django.test import TestCase
from django.urls import reverse
from django.apps import apps
from .apps import PpwConfig
from .models import Makanan, Restoran
from kybe.models import Review
# Create your tests here.

class IndexTest(TestCase):
    def test_html_index(self):
        response = self.client.get(reverse("ppw:index"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'index.html')

class Apps(TestCase):
    def test_apps(self):
        self.assertEqual(PpwConfig.name, 'ppw') 
        self.assertEqual(apps.get_app_config('ppw').name, 'ppw')

class ModelsRestoran(TestCase):
    def create_restoran(self):
        return Restoran.objects.create (
            nama_restoran = "Startbug",
            lokasi = "Jakarta",
            kategori = "Coffe Shop",
            foto = "",
            rating = 0.0
        )
    def test_str(self):
        string = self.create_restoran()
        self.assertEqual(string.__str__(), string.nama_restoran)

class ModelsMakanan(TestCase):
    def create_makanan(self):
        return Makanan.objects.create (
            nama_makanan = "Americano",
            restoran = ModelsRestoran.create_restoran(self),
            harga = 25000,
            foto = "",
        )
    def test_str(self):
        string2 = self.create_makanan()
        self.assertEqual(string2.__str__(), string2.nama_makanan)