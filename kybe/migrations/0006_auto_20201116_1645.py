# Generated by Django 3.1.1 on 2020-11-16 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kybe', '0005_review_rating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='rating',
            field=models.DecimalField(decimal_places=1, default='0.0', max_digits=2),
        ),
    ]
