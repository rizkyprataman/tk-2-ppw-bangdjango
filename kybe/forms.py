from django import forms
from .models import Review
class Pencarian(forms.Form):
    cari = forms.CharField(label="Cari restoran", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cari restoran',
        'type' : 'text',
        'required': True,
    }))

class TambahReview(forms.Form):
    nama = forms.CharField(label="Nama", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama',
        'type' : 'text',
        'required': True,
    }))
    rating = forms.DecimalField(label="Rating", max_digits=2, decimal_places=1, max_value= 5, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': '1.0 - 5.0',
        'required': True,
    }))
    komentar = forms.CharField(label="komentar", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Komentar',
        'type' : 'text',
        'required': True,
    }))
    