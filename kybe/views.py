from django.shortcuts import render, redirect
from .models import Review
from ppw.models import Restoran, Makanan
from .forms import Pencarian, TambahReview
from django.contrib import messages
from ppw.forms import Pembelian
from keranjang.models import Beli
from django.core import serializers
from django.http import JsonResponse, HttpResponse

# Create your views here.
def restoran(request):
    if request.method ==  "POST":
        form = Pencarian(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['cari']
        return redirect('kybe:cariresto', pk = nama)

    else:
        allResto = Restoran.objects.all()
        formPencarian = Pencarian
        isi = {
            'form' : formPencarian,
            'resto' : allResto
        }
        return render(request, 'search.html', isi)

def cariResto(request,pk) :
    if request.method ==  "POST":
        form = Pencarian(request.POST)
        if form.is_valid():
            nama =form.cleaned_data['cari']
        return redirect('kybe:cariresto', pk=nama)
    else: 
        resto = Restoran.objects.filter(nama_restoran__icontains = pk)
        isi = {
            'form' : Pencarian,
            'resto' : resto,
            'nama' : pk
        }
        return render(request, 'search.html', isi)

def resto(request,pk):
    if request.method == "POST":
        form = TambahReview(request.POST)
        formbeli = Pembelian(request.POST)
        namapencarian = request.POST.get("search")
        if form.is_valid():
            reviewer = Review()
            reviewer.reviewer = form.cleaned_data['nama']
            reviewer.isi = form.cleaned_data['komentar']
            reviewer.rating = form.cleaned_data['rating']
            cari = Restoran.objects.get(id=pk)
            reviewer.resto = cari
            reviewer.save()
            messages.success(request, (f"Review Berhasil Ditambahkan!"))
            return redirect('kybe:resto',pk)
        if formbeli.is_valid():
            pembelian = Beli()
            makan = Makanan.objects.get(id=pk)
            pembelian.makanan = makan
            pembelian.jumlah = formbeli.cleaned_data['beli']
            return redirect('kybe:resto',pk)
        if namapencarian != "":
            return redirect('kybe:cariresto', pk = namapencarian)
        else:
            messages.warning(request, (f"Mohon masukan review dengan benar"))
            return redirect('kybe:resto',pk)
    else:
        cari = Restoran.objects.get(id=pk)
        makanan = Makanan.objects.filter(restoran=pk)
        reviews = Review.objects.all()
        form = TambahReview
        form2 = Pencarian
        form3 = Pembelian
        response = {'resto' : cari,
                    'reviews' : reviews,
                    'form' : form,
                    'formpencarian' : form2,
                    'makanan' : makanan,
                    'beli' : form3
        }
        return render(request,'restoran.html', response)

def restorans(request,pk):
    q = request.GET.get('q')
    resto= Restoran.objects.filter(nama_restoran__icontains = q)
    data = serializers.serialize('json', resto)
    return HttpResponse(data, content_type="text/json-comment-filtered")
