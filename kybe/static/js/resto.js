$(document).ready(function(){
    $('#foto-resto').hover(function() {
        $("#foto-resto").addClass('transition');
    }, function() {
        $("#foto-resto").removeClass('transition');
    });

    $('#add-review').click(function() {
        $('#form-review').toggle();
    });
    $('#add-review').hover(function() {
        $(this).css("cursor", "pointer");
    });

    $("#input").on("keyup", function() {
        var q = $(this).val();
        $.ajax({
            url: 'cari/',
            data:{
                'q':q
            },
            datatype:'json',
            success: function(data) {
                if(q.length==0){
                    $('#konten').hide();
                }  
                else{
                    $('#konten').html('')
                    var result =
                    "<tr>"
                    +'<td>'+"<a href=/cari/hasil/"+data[0].pk+">"+"<img class='img-fluid' style='width:15vh' src='" +data[0].fields.foto + "'></img>"+"</a>"+'</td>'
                    +'<td>'+"<a href=/cari/hasil/"+data[0].pk+">"+data[0].fields.nama_restoran+"</td>"+"</a>"
                    +"</tr>"
                    $('#konten').append(result);
                    $('#konten').show();
                }
            }
        });
    });

});