from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Review
from ppw.models import Restoran, Makanan
from django.apps import apps
from kybe.apps import KybeConfig

# Create your tests here.
class TestViews(TestCase):
    def test_index_and_status_code(self):
        response = self.client.get(reverse("kybe:allresto"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "search.html")

    def testSearch(self):
        response = Client().post(reverse("kybe:allresto"), {
            "cari":"ok"
        }, follow=True)
        self.assertContains(response, 'Maaf "ok" tidak ditemukan')

    def testReview_testSearch(self):
        test_resto = Restoran.objects.create(
            nama_restoran = "chatime",
            lokasi = "jakarta",
            kategori = "boba",
            foto = "https://www.centralparkjakarta.com/wp-content/uploads/2017/11/Chatime-photo-1.jpg",
            rating ="0.1"
        )
        url =reverse("kybe:resto",args=[test_resto.id])
        response = Client().post(url, {
            "nama" : "kybe",
            "rating" : "4.5",
            "komentar" : "enak"
        }, follow =True)
        self.assertContains(response, "kybe")
        response2 = Client().post(url, {
            "search":"cha"
            }, follow =True)
        self.assertContains(response2, "chatime")
        url2=reverse("kybe:cariresto", args=["cha"])
        response3 = Client().post(url2, {
                "cari":"cha"
            }, follow=True)
        self.assertContains(response3, "chatime")

    def test_apps(self):
        self.assertEqual(KybeConfig.name, 'kybe')
        self.assertEqual(apps.get_app_config('kybe').name, 'kybe')

    def test_review_makanan(self):
        test_resto = Restoran.objects.create(
            nama_restoran = "chatime",
            lokasi = "jakarta",
            kategori = "boba",
            foto = "https://www.centralparkjakarta.com/wp-content/uploads/2017/11/Chatime-photo-1.jpg",
            rating ="0.1"
        )
        review = Review.objects.create(
            reviewer = "kuma",
            isi ="enak",
            rating =4.5,
            resto=test_resto
        )
        self.assertEqual(str(review), review.reviewer)
    
    def test_json(self):
        response = self.client.get("/cari/hasil/1/cari/?q=")
        self.assertEqual(response.status_code,200)
        


