# Tugas Kelompok PPW

# Pipeline Status: 
[![pipeline status](https://gitlab.com/Rafauzan29/tk-ppw-bangdjango/badges/master/pipeline.svg)](https://gitlab.com/Rafauzan29/tk-ppw-bangdjango/-/commits/master)

# Kelompok BangDjango:
1. Aryuda Oktawyandra - 1906307542
2. Futuh Nurmuntaha Alfahmi - 1906305801
3. Rafidan Musyaffa Irawan - 1906302863
4. Rizky Achmad Fauzan - 1906305713
5. Rizky Pratama Nataprawira - 1906318514

# Link Heroku App:
http://bangdjango2.herokuapp.com/

# Tentang Web:
Pandemi membuat banyak bisnis kuliner gulung tikar, para konsumen juga kesulitan untuk mencari makanan di luar.
Kami membuat web yang dapat menghubungkan antara pembeli dan penjual, seorang pembeli tidak perlu keluar rumah untuk mendapatkan makanan, cukup mengakses web kami di laptop atau smartphone masing-masing.

# FITUR WEBSITE :

## Landing Page
    - Bar pencarian
    - Koleksi (Sarapan, Makan Siang, Makan Malam)(Tipe Restoran)(Kota-kota besar)
    - Rekomendasi makanan
    - Tambahan-tambahan fitur

## Fitur Sign in & Sign Up
    - Membuat akun baru atau pakai Gmail
    - Log in
    - Profile
    - Unlocking feature.
    - Notifikasi
    - Find Friends
    - Log out

## Pencarian
    - Filter Wilayah
    - Koleksi restoran
    - Rating restoran 
    - Rekomendasi restoran

## Pembelian Keranjang
    - Keranjang (item yang dipilih)
    - Konfirmasi pembayaran
    - Metode pembayaran
    - Mengulas setelah produk sampai

## Finishing
    - Navbar atas
    - Footbar
    - About us
    - Help Center
