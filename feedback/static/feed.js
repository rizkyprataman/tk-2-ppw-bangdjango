$(document).ready(function(){

    var csrfToken = $("input[name=csrfmiddlewaretoken]").val();

    $("#addButton").click(function(){
        var serializedData = $("#feedbackForm").serialize();

        $.ajax({
            url : $("feedbackForm").data('url'),
            data: serializedData,
            type: 'post',
            success: function(response) {
                $("#feedbackList").append('<div class="card mb-1" id="taskCard" data-id="' + response.feedb.id + '"><div class="card-body">'+ 
                response.feedb.title +
                '<button type="button" class="close float-right" data-id="' + response.feedb.id +'"><span aria-hidden="true">&times;</span></button></div></div>')
            }
        })

        $("#feedbackForm")[0].reset(); 

    });

    $("#feedbackList").on('click', '.card', function(){
        var dataId = $(this).data('id');
        console.log(dataId)
        $.ajax({
            url: '/feedback/' + dataId + '/completed/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            success: function(){
                var cardItem = $('#feedbackCard[data-id="' + dataId + '"]');
                cardItem.css('text-decoration', 'line-trough').hide().slideDown();
                $('#feedbackList').append(cardItem);
            }
        });
    }).on('click', 'button.close', function(event) {
        event.stopPropagation();

        var dataId = $(this).data('id');

        $.ajax({
            url: '/feedback/' + dataId + '/delete/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            dataType: 'json',
            success: function(){
                $('#feedbackCard[data-id="' + dataId + '"]').remove();
            }
        })
    });
});