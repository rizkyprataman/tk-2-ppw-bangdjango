from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import Feedback
from .forms import FeedbackForm
from .views import FeedbackList, FeedbackComplete, FeedbackDelete
from django.apps import apps
from .apps import FeedbackConfig
from django.contrib.auth.models import User



# class TestApps(TestCase):
#     def test_apps(self):
#         self.assertEqual(Feedback.name, 'feedback') 
#         self.assertEqual(apps.get_app_config('feedback').name, 'feedback')


class ModelTest(TestCase):
    def setUp(self):
        self.Feedback = Feedback.objects.create(title="amazing")
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')
    
    def test_model_dibuat(self):
        self.assertEqual(Feedback.objects.count(), 1)

    def test_str(self):
        self.assertEquals(str(self.Feedback), "amazing")


class FormTest(TestCase):
    def setUp(self):
        self.Feedback = Feedback.objects.create(title="amazing")
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')

    def test_form_invalid(self):
        form = FeedbackForm(data={})
        self.assertFalse(form.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_FEEDBACK_ada(self):
        response_get = Client().get("/feedback/", {"title": "amazing"})
        self.assertEqual(response_get.status_code, 200)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.req = reverse("feedback:req_list_url")
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')

    def test_views_feedback(self):
        response = self.client.get(self.req, {'title': 'amazing'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedback/feedback.html')