from django.shortcuts import render, redirect
from .models import Feedback
from .forms import FeedbackForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, auth
from django.views.generic import View
from django.http import JsonResponse
from django.forms.models import model_to_dict
# Create your views here.

class FeedbackList(View):

    def get(self, request):
        form = FeedbackForm()
        req = Feedback.objects.all()
        return render(request, 'feedback/feedback.html', context={'form': form, 'req':req})

    # @login_required
    def post(self, request):
        form = FeedbackForm(request.POST)
        if form.is_valid():
            new_feedback = form.save()
            return JsonResponse({'feedb': model_to_dict(new_feedback)}, status=200)
        else:
            return redirect('feedback:req_list_url')


class FeedbackComplete(View):
    def post(self, request, id):
        feedback_done = Feedback.objects.get(id=id)
        feedback_done.completed = True
        feedback_done.save()
        return JsonResponse({'feedb': model_to_dict(Feedback)}, status=200)


class FeedbackDelete(View):
    def post(self, request, id):
        feedback = Feedback.objects.get(id=id)
        feedback.delete()
        return JsonResponse({'result': 'ok'}, status=200)