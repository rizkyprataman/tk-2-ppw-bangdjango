from django import forms
from .models import Feedback
from django.contrib.auth.models import User


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['title']

        widget = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'style': "width: 100%"})
        }
