from django.contrib import admin
from django.urls import path
from .views import FeedbackList, FeedbackComplete, FeedbackDelete
from django.conf import settings
from django.conf.urls.static import static

app_name = 'feedback'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', FeedbackList.as_view(), name='req_list_url'),
    # path('request/', RequestList.as_view(), name='req_list_url'),
    path('<str:id>/completed/', FeedbackComplete.as_view(), name="req_complete_url"),
    path('<str:id>/delete/', FeedbackDelete.as_view(), name="req_delete_url"),
    ]