from django.db import models
from django.conf import settings

# Create your models here.

class Feedback(models.Model):
    title = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    completed = models.BooleanField(default=False);

    def __str__(self):
        return self.title;

    class Meta:
        ordering = ['completed', 'date']