from django import forms
from .models import konfirmasi
from .models import PMB_CHOICES
from .models import PGM_CHOICES

class KonfirmasiForm(forms.ModelForm):
    class Meta:
        model = konfirmasi
        fields = ['alamat','pembayaran','pengiriman','noHP','catatan']
        widgets = {
            'alamat': forms.TextInput(attrs={'class':'form-control'}),
            'pengiriman': forms.Select(choices=PGM_CHOICES,attrs={'class':'form-control'}),
            'pembayaran': forms.Select(choices=PMB_CHOICES,attrs={'class':'form-control'}),
            'noHP': forms.TextInput(attrs={'class':'form-control'}),
            'catatan': forms.TextInput(attrs={'class':'form-control'})
        }