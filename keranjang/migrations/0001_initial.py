# Generated by Django 3.1.2 on 2020-11-20 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='konfirmasi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alamat', models.CharField(max_length=1000, verbose_name='Alamat Pengiriman')),
                ('pembayaran', models.CharField(choices=[('Go-pay', 'Go-bay'), ('Ovo', 'Owo'), ('Debit', 'Debit')], default='Debit', max_length=50, verbose_name='Metode Pembayaran')),
                ('pengiriman', models.CharField(choices=[('JNO', 'Go-pay'), ('TUKI', 'Ovo'), ('Si Lambat', 'Debit')], default='Si Lambat', max_length=50, verbose_name='Metode Pengiriman')),
                ('noHP', models.IntegerField()),
                ('catatan', models.CharField(blank=True, max_length=500, verbose_name='Catatan')),
            ],
        ),
    ]
