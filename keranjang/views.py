from django.shortcuts import render, redirect
from ppw.models import Restoran, Makanan
from .models import konfirmasi, Beli
from .forms import KonfirmasiForm

# Create your views here.
def keranjang(request):
    keranjang = Makanan.objects.all()
    resto = Restoran.objects.all()
    args = {'makan':keranjang, 'resto':resto}
    return render(request, 'keranjang_landing.html', args)

def checkout(request):
    if request.method == "POST":
        form = KonfirmasiForm(request.POST)
        if form.is_valid() :
            data_konfirmasi = form.save()
            data_konfirmasi.save()
            return redirect('keranjang:bayar')
    else :
        form = KonfirmasiForm()
        makanan = Makanan.objects.all()
        args = {'show':makanan, 'form':form}
    return render(request, 'checkout.html', args)

def bayar(request):
    return render(request, 'bayar.html')

def detail(request):
    detail = konfirmasi.objects.order_by("noHP")
    response = {'detail':detail} 
    return render(request,'detail.html',response)



